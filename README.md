# DiabetesST2004Sim
This is the product of an optional project I developed whilst taking a course on Applied Probability (ST2004) as a 3rd year Electronic and Computer Engineering visiting student at Trinity College Dublin, Michaelmas term, 2013, under the supervision of Dr. Brett Houlding.

I simulated the spread of type-1 diabetes on a population considering several population parameters such as the initial proportion of diabetics, women's fertility, the proportion of mothers under the age of 25 (whose children are more likely to develop the disease), an exchange factor (to represent population dynamics) and the average number of partners each woman may have children with over her life. I used information gathered by the American Diabetes Association (ADA) in order to make my simulation more representative of the real world.

The simulation was coded in Java (under Eclipse) and the results were exported as CSV files to be analysed using a spreadsheet processor.

# How to
I recommend you import this project to Eclipse (or some other IDE of your choice) after downloading it.

In order to modify population parameters, which in practice is all that matters to change simulation outcomes, all you are supposed to do is edit the class util.Constants. The constants it defines are the following:

POPULATION_SIZE: the initial population size; DIABETICS: the initial proportion of diabetics; ODDS_FATHER_ONLY: chances of an individual to become diabetic if only his father is diabetic; ODDS_MOTHER_ONLY_LESS_THAN_TWENTY_FIVE: chances of an individual to become diabetic if only his mother is diabetic and she is under 25 years of age; ODDS_MOTHER_ONLY_NOT_LESS_THAN_TWENTY_FIVE: chances of an individual to become diabetic if only his mother is diabetic and she is over 25 years of age; WOMEN_S_FERTILITY: women's fertility (i.e. the number of kids each woman gives birth to over her life); MOMS_UNDER_AGE_25: proportion of mothers under 25 years of age; GENERATIONS: number of generations simulated; EXCHANGE_FACTOR: population exchange factor

The default values defined for these constants were defined based on ADA data.

Once you are happy with the simulation parameters you define, just run it and the results will be exported to the CSV file named at diabetesinheritance.DiabetesInheritance (line 50).

# Contact
Do not hesitate to contact me for further information regarding this project. I will happily answer your query. Please visit https://sites.google.com/site/emedosvasc/work/a-simulation-study-on-diabetes for a detailed project report and contact info.
